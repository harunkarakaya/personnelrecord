#include <stdio.h>
#include <stdlib.h>
typedef struct
{
    char isim[50];
    int yas;
    int maas;
}Kisi;

int Kayitekle()
{
    FILE *fk_ekle;
    Kisi kayitekle;
    char deger;
    int sayac=0;
    if((fk_ekle=fopen("per.dat","ab+"))==NULL)
    {
        printf("Dosya acma hatasi.");
        exit(1);
    }
    while(1==1)
    {
        printf("Isim:");
        scanf("%s",kayitekle.isim);
        printf("Yas:");
        scanf("%d",&kayitekle.yas);
        printf("Maas:");
        scanf("%d",&kayitekle.maas);
        sayac++;
        if(fwrite(&kayitekle,sizeof(Kisi),1,fk_ekle)!=1)
        {
            printf("Yazma hatasi.");
            exit(1);
        }
        printf("Yeni kayit icin y yoksa n <y/n>:");
        scanf("%s",&deger);
        if(deger == 'n' || deger =='N')
        {
            break;
        }
    }
    fclose(fk_ekle);
    return sayac;
}

void Listele()
{
    FILE *flistele;
    Kisi listele;
    if((flistele=fopen("per.dat","rb+"))==NULL)
    {
        printf("Okuma hatasi.");
        exit(1);
    }
    while(fread(&listele,sizeof(Kisi),1,flistele)==1)
    {
        printf("%s %d %d\n\n",listele.isim,listele.yas,listele.maas);
    }
    fclose(flistele);
}

int Duzenle(int sayac)
{
    FILE *fduzenle;
    Kisi duzenle;
    char Ad[100],x;
    int i,imlec=0,a=0;
    if((fduzenle=fopen("per.dat","rb+"))==NULL)
    {
        printf("Okuma hatasi.");
        exit(1);
    }
    while(1==1)
    {
        printf("Duzenlemek Istediginiz Kayit Ismi:");
        scanf("%s",Ad);
        while(a==0)
        {
            if(fread(&duzenle,sizeof(Kisi),1,fduzenle)!=1)
            {
                printf("Okuma hatasi.");
                exit(1);
            }
            if(strcmp(duzenle.isim,Ad)==0)
            {
                printf("Yeni Ad,Yas,Maas Bilgilerini Giriniz:");
                scanf("%s",duzenle.isim);
                scanf("%d",&duzenle.yas);
                scanf("%d",&duzenle.maas);
                fseek(fduzenle,(imlec)*sizeof(Kisi),SEEK_SET);
                a=1;
                if(fwrite(&duzenle,sizeof(Kisi),1,fduzenle)!=1)
                {
                    printf("Yazma hatasi.");
                    exit(1);
                }
            }
            imlec++;
        }
        printf("Baska Kayit Guncelleme Yapmak Istiyormusunuz Evet ise y Hayirsa n:");
        scanf("%s",&x);
        if(x == 'y' || x == 'Y')
        {
            a=0;
            imlec=0;
        }
        else
            break;
    rewind(fduzenle);
    }
    fclose(fduzenle);
}

int Sil()
{
    FILE *fsil;
    Kisi sil;
    char Ad[100],x;
    int a=0,imlec=0,i;
    if((fsil=fopen("per.dat","rb+"))==NULL)
    {
        printf("Okuma hatasi.");
        exit(1);
    }
    while(1==1)
    {
        printf("Silmek Istediginiz Kayit Sahibinin Adini Giriniz:");
        scanf("%s",Ad);
        while(a==0)
        {
            if(fread(&sil,sizeof(Kisi),1,fsil)!=1)
            {
                printf("Okuma hatasi.");
                exit(1);
            }
            if(strcmp(sil.isim,Ad)==0)
            {
                for(i=0;i<50;i++)
                {
                    sil.isim[i] = NULL;
                }
                sil.yas = NULL;
                sil.maas = NULL;
                fseek(fsil,(imlec)*sizeof(Kisi),SEEK_SET);
                a=1;
                if(fwrite(&sil,sizeof(Kisi),1,fsil)!=1)
                {
                    printf("Yazma hatasi.");
                    exit(1);
                }
            }
            imlec++;
        }
        printf("Baska Kayit Silmek Isermisiniz y yoksa n:");
        scanf("%s",&x);
        if(x == 'y' || x=='Y')
        {
            imlec=0;
            a=0;
        }
        else
            break;
        rewind(fsil);
    }
    fclose(fsil);
}
int main()
{
    int secim,sayac;
    while(secim !=5)
    {
    printf("1.Kayit Ekle\n");
    printf("2.Listele\n");
    printf("3.Duzenle\n");
    printf("4.Sil\n");
    printf("5.Cikis\n");
    printf("Secimini Yap <1-5>:");
    scanf("%d",&secim);
    if(secim == 1)
    {
        sayac=Kayitekle();
    }
    if(secim == 2)
    {
        Listele();
    }
    if(secim == 3)
    {
        Duzenle(sayac);
    }
    if(secim == 4)
    {
        Sil();
    }
    if(secim == 5)
    {
        printf("Cikis Yaptiniz.");
    }
    }
}
